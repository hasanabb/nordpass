import { createStore } from "vuex";

export default createStore({
	state: {
		alt: "password, password manager, password manager review, password manager download كلمة المرور ، مدير كلمات المرور ، مراجعة مدير كلمات المرور ، تنزيل مدير كلمات المرور",
	},
	mutations: {},
	actions: {},
	modules: {},
	getters: {
		alt(state) {
			return state.alt;
		},
	},
});
