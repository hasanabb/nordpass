import { createI18n } from "vue-i18n";
import en from "./localse/en.json";
import ar from "./localse/ar.json";

export default createI18n({
	locale: "en",
	fallbackLocale: "en",
	messages: { en, ar },
});
