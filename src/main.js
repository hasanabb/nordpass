import { createApp } from "vue";
import App from "./App.vue";
import store from "./store.js";
import i18n from "./i18n.js";

// import 'bootstrap';
// import 'bootstrap/dist/css/bootstrap.min.css';

createApp(App)
	.use(store)
	.use(i18n)
	.mount("#app");
